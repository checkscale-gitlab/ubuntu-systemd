# ubuntu-systemd

A Docker image based on Ubuntu including a minimal systemd
with cron, logrotate & rsyslog services.

## Setup

* Install docker: https://docs.docker.com/engine/installation/
* Install docker-compose: https://docs.docker.com/compose/install/
* Build the docker image: `./docker-compose.sh build`
* Start the container: `./docker-compose.sh up -d`
* Enter the container: `./docker-compose.sh exec app bash`
* Stop the container: `./docker-compose.sh down`
